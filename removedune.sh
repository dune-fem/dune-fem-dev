dockerName="registry.dune-project.org/dune-fem/dune-fem-dev"

confirm() {
  read -p "Continue (y/n)?" choice
  case "$choice" in
    y|Y ) return 0;;
    n|N ) return 1;;
    * ) echo "invalid";;
  esac
  return 1
}

if [ "$(docker container ls -a | grep dunepy)" ] ;
then
  echo "removing dunepy container"
  if confirm ;
  then
    docker container rm dunepy
  else
    exit 0
  fi
fi
if [ "$(docker volume ls | grep dunepy)" ] ;
then
  echo "removing dunepy volume"
  if confirm ;
  then
    docker volume rm dunepy
  else
    exit 0
  fi
fi
if [ "$(docker image ls | grep $dockerName)" ] ;
then
  echo "removing $dockerName image"
  if confirm ;
  then
    docker image rm $dockerName
  else
    exit 0
  fi
fi
if [ "$(docker image ls | grep ubuntu)" ] ;
then
  echo "removing ubuntu base image"
  confirm && docker image rm ubuntu:18.04
fi
