#!/bin/bash

# cron-job script to build and push the images

ACTION=${1:-all}
VERBOSE=false
DRY=""
PACKAGE_NAME=DuneFemDev
REGISTRYUSER=docker-image-builder
REGISTRYPASS=$(cat $HOME/.docker/dune-fem-dev-registrypass)
REGISTRY=registry.dune-project.org
REPOSITORY=registry.dune-project.org/dune-fem/dune-fem-dev
TAGS="
  base
  2.7
  latest
"

LOGFILE=$(mktemp)
SOMETHING_FAILED=""

function cleanup()
{
    rm -f $LOGFILE
}

trap cleanup EXIT

if ! $VERBOSE; then
    exec 6>&1 7>&2 1> $LOGFILE 2>&1
    MSGFD=6
    ERRFD=6
else
    MSGFD=1
    ERRFD=2
fi

set -xe

cd $(dirname $(realpath $0))

git pull || SOMETHING_FAILED="git pull"

if ! [ "$ACTION" = "push" ]; then
    if ! bash -x ./build.sh full; then
	SOMETHING_FAILED="docker build failed"
    fi
fi

if ! [ "$ACTION" = "build" ]; then
    echo $REGISTRYPASS | $DRY docker login -u $REGISTRYUSER --password-stdin $REGISTRY || REGISTRY_FAILURE=true
    for tag in $(echo $TAGS); do
	IMAGETAG=$REPOSITORY:$tag
	$DRY docker push $IMAGETAG || SOMETHING_FAILED="docker push failed"
    done
fi

if [ -n "$SOMETHING_FAILED" ]; then
    echo "**************** $PACKAGE_NAME Docker Image Build Failed ****************************" 1>&$ERRFD
    echo 1>&$ERRFD
    cat $LOGFILE 1>&$ERRFD
else
    echo "**************** $PACKAGE_NAME Docker Image Build Succeeded ****************************" 1>&$MSGFD
fi
