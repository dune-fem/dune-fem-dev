#!/bin/bash

#change appropriately, i.e. 2.7 or empty which refers to master
DUNEVERSION=$1

if [ "$DUNEVERSION" != "" ] && ["$DUNEVERSION" != "latest"]; then
  DUNEVERSION="release $DUNEVERSION"
else
  DUNEVERSION="devlopment version"
fi

echo "\

#####################################################################################
#####################################################################################
source \$HOME/dune-env/bin/activate
export DUNE_LOG_FORMAT='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
export DUNE_LOG_LEVEL=CRITICAL

#####################################################################################

# function for reconfiguring all dune-fem related modules
function updateFem {
  DUNEFEMMODULES=\"dune-fem dune-fem-dg dune-vem\"
  thisDir=\$PWD
  cd ~/DUNE
  for M in \$DUNEFEMMODULES ; do
    ./dune-common/bin/dunecontrol --only=\$M git pull
  done
  cd \$thisDir
}
# function for updating all Dune modules
function updateDune {
  thisDir=\$PWD
  cd ~/DUNE
  ./dune-common/bin/dunecontrol git pull
  ./dune-common/bin/dunecontrol --opts=config.opts all
  cd \$thisDir
}
# function for updating the Dune Python environment
# warning: calling this function removes all the cached Python modules in # dune-py
function updatePython {
  thisDir=\$PWD
  cd ~/DUNE
  ./dune-python/bin/setup-dunepy.py --opts=config.opts install
  cd \$thisDir
}
# function for rebuilding the full Dune stack, including rebuilding the
# Dune modules and the Dune Python environment (also clears the dune-py # cache)
function updateAll {
  updateDune
  updatePython
}

# avoid warning
export XDG_RUNTIME_DIR=/tmp/runtime-dune

# make sure virtualenv is available in ipython
alias ipython=\"python -c 'import IPython; IPython.terminal.ipapp.launch_new_instance()'\"
alias duneversion=\"echo image based on $DUNEVERSION\"

cd /host
" >> .bashrc

echo "\
\"\e[A\": history-search-backward
\"\e[B\": history-search-forward
" >> .inputrc

source buildDune.sh -b $1
