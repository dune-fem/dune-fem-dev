# warning: this removes all docker related images,container,volumes so be # careful...
# docker system prune -a -f --volumes
# docker rmi $(docker images -q)
if [ "$1" != "" ] ; then
  DUNEVERSION="$1"
else
  DUNEVERSION="latest"
fi
dockerName=registry.dune-project.org/dune-fem/dune-fem-dev:$DUNEVERSION

docker volume rm dunepy$DUNEVERSION
docker image rm $dockerName
docker system df

docker build . --no-cache -t $dockerName --build-arg DUNEVERSION="$DUNEVERSION"
