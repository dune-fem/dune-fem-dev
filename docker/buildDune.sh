#!/bin/bash

DUNECOREMODULES="dune-common dune-istl dune-geometry dune-grid dune-localfunctions"
DUNEEXTMODULES="dune-alugrid"
DUNEFEMMODULES="dune-fem dune-fempy dune-fem-dg dune-vem"

while getopts b:p option
do
case "${option}"
in
b) DUNEBRANCH=${OPTARG};;  # either e.g. 2.7 or include -b flag
p) DUNEDIR=${OPTARG};;
esac
done

if [ "$DUNEDIR" == "" ] ; then
  DUNEDIR="$HOME/DUNE"
fi

###########################################

if [ "$DUNEBRANCH" == "2.7" ] ; then
  DUNEBRANCH="-b releases/2.7"
  DUNEVERSION="release $DUNEVERSION"
  DUNEPYTHONMODULE="dune-python"
  DUNEPYSETUP="dune-python/bin/setup-dunepy.py"
elif [ "$DUNEBRANCH" == "" ] || [ "$DUNEBRANCH" = "latest" ]; then # latest
  DUNEBRANCH=
  DUNEVERSION="devlopment version"
  DUNEPYTHONMODULE=""
  DUNEPYSETUP="dune-common/bin/setup-dunepy.py"
else # assume branch is post 2.7, i.e., core python bindings in dune-common
  DUNEVERSION="branch $DUNEBRANCH"
  DUNEPYTHONMODULE=""
  DUNEPYSETUP="dune-common/bin/setup-dunepy.py"
fi

FLAGS="-O3 -DNDEBUG -funroll-loops -finline-functions -Wall -ftree-vectorize -fno-stack-protector -mtune=native"

###########################################

# create necessary python virtual environment
if ! test -d $HOME/dune-env ; then
  # do not use python3 here or Dune will not pick up virtualenv correctly
  python3 -m venv $HOME/dune-env --system-site-packages
  source $HOME/dune-env/bin/activate
  pip install --upgrade pip
  pip install IPython
else
  source $HOME/dune-env/bin/activate
fi

if ! test -d $DUNEDIR ; then
  # we need to put the dune module into a subdirectory otherwise dune-py in
  # the virtual env will be picked up during build of dune
  mkdir $DUNEDIR
  cd $DUNEDIR
  # build flags for all DUNE and OPM modules
  # change according to your needs
  echo "\
  DUNEPATH=`pwd`
  BUILDDIR=build-cmake
  USE_CMAKE=yes
  MAKE_FLAGS=-j4
  CMAKE_FLAGS=\"-DCMAKE_CXX_FLAGS=\\\"$FLAGS\\\"  \\
   -DCMAKE_LD_FLAGS=\\\"$PY_LDFLAGS\\\" \\
   -DALLOW_CXXFLAGS_OVERWRITE=ON \\
   -DENABLE_HEADERCHECK=OFF \\
   -DDUNE_ENABLE_PYTHONBINDINGS=TRUE \\
   -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \\
   -DDUNE_PYTHON_INSTALL_EDITABLE=TRUE \\
   -DADDITIONAL_PIP_PARAMS="-upgrade" \\
   -DPETSC_DIR=/usr/lib/petscdir/petsc3.12/x86_64-linux-gnu-real \\
   -DDISABLE_DOCUMENTATION=TRUE \\
   -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE\" " > config.opts

  # get all necessary dune core modules
  for MOD in $DUNECOREMODULES ; do
    git clone $DUNEBRANCH https://gitlab.dune-project.org/core/$MOD.git
  done
  # get all dune extension modules necessary
  for MOD in $DUNEEXTMODULES ; do
    if [ "$MOD" == "dune-alugrid" ]; then
      git clone $DUNEBRANCH https://gitlab.dune-project.org/extensions/$MOD.git
    else
      git clone $DUNEBRANCH https://gitlab.dune-project.org/staging/$MOD.git
    fi
  done
  if [ "$DUNEPYTHONMODULE" == "dune-python" ]; then
    git clone $DUNEBRANCH https://gitlab.dune-project.org/staging/dune-python.git
  fi
  # get all dune fem modules necessary
  for MOD in $DUNEFEMMODULES ; do
      git clone $DUNEBRANCH https://gitlab.dune-project.org/dune-fem/$MOD.git
  done
else
  cd $DUNEDIR
  ./dune-common/bin/dunecontrol git pull
  rm -rf */build-cmake
fi

# build all DUNE modules using dune-control
./dune-common/bin/dunecontrol --opts=config.opts all

# install all python modules in the pip environment
$DUNEPYSETUP --opts=config.opts install

cd ..
