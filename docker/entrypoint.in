#!/usr/bin/env bash

echo "###############################################################"
echo "# Welcome to the dune-fem docker environment,                 #"
echo "# this container is based on the image: VERSIONAAAA           #"
echo "#                                                             #"

# first check that we have enough memory
python3 -c "
import psutil, sys
if psutil.virtual_memory().total < 3.5 * 1024**2: sys.exit(1)
"
if [ ! $? -eq 0 ]; then
  echo "# Unfortunately your docker configuration does not supply     #"
  echo "# us with enough memory - compiling some of the dune source   #"
  echo "# can take about 2-3GB... Please change the maximum available #"
  echo "# memory to 4GB or more in the advanced docker settings.      #"
  echo "# Sorry about that!                                           #"
  echo "###############################################################"
  exit 1
fi

# if group and user id are provided change access rights
if [ ! $groupId -eq 0 ] && [ ! $userId -eq 0 ] ; then
  # echo "switching user:group id from `id -u dune`:`id -g dune` to $userId:$groupId"
  chgrp -R -h $groupId /dunepy
  chown -R -h $userId /dunepy
  usermod -u $userId dune
  groupmod -o -g $groupId dune
  echo "# The dune git repositories are located at '/dunepy/DUNE' .   #"
  echo "# A python virtual environment is active and Python           #"
  echo "# scripts can be found in '/dunepy/DUNE/dune-fempy/demos'.    #"
  echo "#                                                             #"
  python3 -c "
import psutil, sys
if psutil.cpu_count() == 1: sys.exit(1)
"
  if [ ! $? -eq 0 ]; then
    echo "# There seems to be only one CPU core available to run        #"
    echo "# programs within docker - that works but having some         #"
    echo "# more will improve performance. Consider changing the        #"
    echo "# coresponding docker settings.                               #"
    echo "#                                                             #"
  fi
  echo "# Ctrl+p Ctrl+q detaches from the container without stopping  #"
  echo "# any processes within running inside the container at the    #"
  echo "# time - running this script again reattaches to the running  #"
  echo "# container.                                                  #"
  echo "###############################################################"
  echo "$@"
  exec gosu dune bash "$@"
else
  # echo "not setting any user/group id - will start as root"
  exec bash "$@"
fi
