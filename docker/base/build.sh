if [ "$1" = "destructive" ]; then
    # warning: this removes all docker related images,container,volumes so be # careful...
    docker system prune -a -f --volumes
    docker rmi $(docker images -q)
    docker system df
fi

docker build . --no-cache -t registry.dune-project.org/dune-fem/dune-fem-dev:base
