if [ "$1" == "full" ]
then

  cd base
  bash ./build.sh
  cd ..

  cd release2.7
  cp ../buildDuneDocker.sh ../buildDune.sh ../dockerbuild.sh ../Dockerfile  .
  sed -e s/VERSIONAAAA/'release 2.7'/ < ../entrypoint.in > entrypoint.sh
  bash ./build.sh
  rm buildDuneDocker.sh buildDune.sh dockerbuild.sh Dockerfile entrypoint.sh
  cd ..

fi

cd latest
cp ../buildDuneDocker.sh ../buildDune.sh ../dockerbuild.sh ../Dockerfile .
sed -e s/VERSIONAAAA/development/ < ../entrypoint.in > entrypoint.sh
bash ./build.sh
rm buildDuneDocker.sh buildDune.sh dockerbuild.sh Dockerfile entrypoint.sh
cd ..
