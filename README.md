# Using a Docker or Vagrant Environment for Dune (C++ and Python)

Build a docker or vagrant environment suitable for developing both C++ and Python modules#
Both have to be build locally on the host system and can then be run from
any folder on the host machine, i.e., a folder containing some Dune module
or some Python script. Access rights are set so that the docker/vagrant user can edit
has the same rights as the user who build the docker/vagrant image
in the directory from where it was started; consequently new files
generated during the session are modifiable on the host.

The Linux distribution used for the image is based on a Ubuntu image
and contains most programs needed for shell based code development.
To install additional packages clone this repository and modify the
`bootstrap.sh` script adding appropriate `apt-get install` or `pip install` commands.
After each modification to image has to be rebuild within the cloned repository.
To add additional Dune modules or install additional Python packages modify
the `buildDune.sh` script (after cloning the repo).

## Docker

*Note*: if you have used any of the `dune` docker images before you should
remove the old `dune` volume used in previous versions:
```
docker volume rm dune
```

The easiest way to get started is to download the `rundune.sh` script
```
wget https://gitlab.dune-project.org/dune-fem/dune-fem-dev/raw/master/rundune.sh
```
For MS Windows with recent installs of the Docker Desktop please use the
batch-file `rundune.bat` instead:
```
wget https://gitlab.dune-project.org/dune-fem/dune-fem-dev/raw/master/rundune.bat
```

This scripts starts a docker container (after downloading the images if
necessary). If no argument is passed to the script the latest release
version `2.7`. The argument `latest` can be added to access the latest
non stable version of the container using the master branches of the
core modules.

The following describes the steps used in the [**script**](rundune.sh) to start
the *dune-fem docker development environment*. Execute the script in the
folder containing the Python scripts or Dune C++ modules to download and
run the Docker image. The script contains the following run command
```
docker run -it --rm -v $PWD:/host -v dunepy:/dunepy \
  -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
  -e userId=$(id -u) -e groupId=$(id -g) registry.dune-project.org/dune-fem/dune-fem-dev:latest
```
The second line in the `docker run` command is included to activate X forwarding on
Linux machines. To get it to work run either `xhost +` or for more security
```
xhost +si:localuser:$USER
```
before starting the container.  For Windows and MAC OS a bit more work is required as
discussed below.

The current directory will be mounted as `/host` and the main Dune modules and
the Python virtual environment will be located under the home directory (`/dunepy`)
of the `dune` user located in the corresponding data volume. The git
repositories of all Dune modules included in the image are then available under the
home directory `dunepy/DUNE`. These are stored in the `dunepy` volume so
that changes can be made persistently, i.e., updating the modules,
switching branches, or adding additional modules. Of course any changes
will requires using `dunecontrol` and `dune-setup.py` to rebuild the `dune`
environment.

### MAC users
To get X forwarding to work in Docker requires
additionally `xquartz` and `socat` as discussed [here][socat].
If somebody knows of an easier fix please let us know...
In summary (but please check the given website):

- install XQuartz (X11) and socat
- in a separate terminal run
```
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"
```
- then execute the `rundune.sh` script

### Note for Windows users
To get X forwarding you will need an x server running. For example
install `vcxsrv`, start it with `xlaunch` and during initial
configuration tick `Disable access control`. After that running the
`rundune` script or batch-file should work. If there is still an issue with X
forwarding try setting the `DISPLAY` environment variable to the IP address
of your Windows machine.

- a recent Docker Desktop on recent versions of Windows 10 uses the
 [WSL 2](https://docs.docker.com/docker-for-windows/wsl/) engine
  which is also used by Windows for its virtual Ubuntu sub-system. In this case
  you should use the batch-file `rundune.bat` directly from the powershell app.
  It should just work.
- older versions of Docker with older versions of Windows 10 will (perhaps) need
  the *Docker Toolbox* installed (which now is deprecated). It may work to use
  the `rundune.sh` script from an installed *Git-Bash*. These old Docker instals
  are based on the VirtalBox app. In this case the memory usage is restricted to
  1GB by default for the virtual box. Open the virtual box app, stop any running
  machines and then change the setting so that at 4GB are available. At the
  same time increasing the number of CPUs could further improve performance.
  After that restart the virtual machine, restart the docker terminal and
  hopefully everything works.

## Vagrant
Run `vagrant up` to get started and then `vagrant ssh` in your working
directory to go into the container.
To update the container run `vagrant provision` which will re-execute the
`bootstrap.sh` file.

[socat]: https://irvingduran.com/2017/07/docker-container-x11-on-macos-awesome/


# To Build the Docker Images
This repository also contains the Dockerfiles and scripts to build the
docker images. These consists of a base image, release versions (e.g. 2.7),
and the development image. The scripts are located in the `docker` folder.

`docker/base` contains the files needed for the `base` image.  The folder
only contains a `Dockerfile` and script `build.sh`.

Release and development images are based on the scripts in the `docker` folder:

-  `dockerbuild.sh`: main script to build a single image
-  `buildDune.sh`: main script run as user during building the docker image
   which contains the setup of the virtual environment and the of the dune
   repositories.
-  `Dockerfile`: the docker file for the development/release images
-  `entrypoint.in`: message shown when docker container is started

The script `build.sh` is the main build script which by default builds the development
image. If `full` is passed as argument, the `base` and `release` images are
also rebuild. To build the development or release images, the files
described above into the `latest` (and `releaseXY`) folders
which also contain a `build.sh` script which runs `dockerbuild.sh` using a
parameter to fix the image to build, e.g.,
`bash ./dockerbuild.sh 2.7`
No parameter leads to the build of the `latest` image based on the master
branches of the dune modules.
